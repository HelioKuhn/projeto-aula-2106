package br.com.syonet.concessionaria;

public class Carro extends Veiculo{
	
	private TipoCombustivel tipoCombustivel;
	private Boolean direcaoHidraulica;
	private Integer quantidadePassageiros;
	private TipoCambio tipoCambio;
	
	
	
	public Carro(Integer quantRodasInteger, Double peso, String cor, Integer ano, String marca, String modelo, Double preco,
			NovoUsado novoUsado, String numeroChassi, TipoCombustivel tipoCombustivel, Boolean direcaoHidraulica, Integer quantidadePassageiros, TipoCambio tipoCambio) {
		super(quantRodasInteger, peso, cor, ano, marca, modelo, preco, novoUsado, numeroChassi);
		this.tipoCombustivel = tipoCombustivel;
		this.direcaoHidraulica = direcaoHidraulica;
		this.quantidadePassageiros = quantidadePassageiros;
		this.tipoCambio = tipoCambio;
	}

	
	public  String toString() {
		return "Carro\n"
				+ super.toString()
				+"Tipo Combustivél: " + getTipoCombustivel() + "\n"
				+"Direção Hidraulica: " + getDirecaoHidraulica() + "\n"
				+"Quantidade Passageiros: " + getQuantidadePassageiros() + "\n"
				+"Tipo de Cambio: " + getTipoCambio()
				+"\n\n";
	}


	public TipoCombustivel getTipoCombustivel() {
		return tipoCombustivel;
	}



	public void setTipoCombustivel(TipoCombustivel tipoCombustivel) {
		this.tipoCombustivel = tipoCombustivel;
	}



	public String getDirecaoHidraulica() {
		return direcaoHidraulica == true ?  "SIM" : "NÃO";
	}



	public void setDirecaoHidraulica(Boolean direcaoHidraulica) {
		this.direcaoHidraulica = direcaoHidraulica;
	}



	public Integer getQuantidadePassageiros() {
		return quantidadePassageiros;
	}



	public void setQuantidadePassageiros(Integer quantidadePassageiros) {
		this.quantidadePassageiros = quantidadePassageiros;
	}



	public TipoCambio getTipoCambio() {
		return tipoCambio;
	}



	public void setTipoCambio(TipoCambio tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	

}
