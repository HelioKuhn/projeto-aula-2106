package br.com.syonet.concessionaria;

public enum TipoCombustivel {
	
	GALOSINA,
	ALCOOL,
	FLEX
}
