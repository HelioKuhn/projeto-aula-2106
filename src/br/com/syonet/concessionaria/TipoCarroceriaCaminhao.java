package br.com.syonet.concessionaria;

public enum TipoCarroceriaCaminhao {
	
	GRANEIRO,
	BAU,
	BAU_FRIGORIFICO,
	PLATAFORMA,
	TANQUE,
	CACAMBA

}
