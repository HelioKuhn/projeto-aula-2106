package br.com.syonet.concessionaria;

import java.util.List;

public interface Concessionaria {

	void adicionarVeiculoEstoque(Veiculo veiculo);

	void removerVeiculoEstoque(Veiculo veiculo);

	void atualizarPrecoVenda(Veiculo veiculo, Double preco);

	void venderVeiculo(Veiculo veiculo, TipoCliente tipoCliente);

	List<Carro> listarCarros(NovoUsado novoUsado);

	List<Caminhao> listarCaminhoes(TipoCarroceriaCaminhao tipoCarroceriaCaminhao);

	List<Moto> listarMotosMarca(String marca);

	List<Moto> listarMotosCilindrada(Integer cilindrada);
	
	void listarVeiculosVendidos();

}
