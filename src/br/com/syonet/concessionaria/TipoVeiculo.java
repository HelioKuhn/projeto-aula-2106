package br.com.syonet.concessionaria;

public enum TipoVeiculo {

	CARRO,
	MOTO,
	CAMINHAO
}
