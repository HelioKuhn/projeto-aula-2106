package br.com.syonet.concessionaria;

public class Veiculo {
	
	private Integer quantRodas;
	private Double peso;
	private String cor;
	private Integer ano;
	private String marca;
	private String modelo;
	private Double preco;
	private NovoUsado novoUsado;
	private String numeroChassi;
	
	
	public Veiculo(Integer quantRodas, Double peso, String cor, Integer ano, String marca, String modelo, Double preco,
			NovoUsado novoUsado, String numeroChassi) {
		super();
		this.quantRodas = quantRodas;
		this.peso = peso;
		this.cor = cor;
		this.ano = ano;
		this.marca = marca;
		this.modelo = modelo;
		this.preco = preco;
		this.novoUsado = novoUsado;
		this.numeroChassi = numeroChassi;
	}

	
	public String toString() {
		return "Marca: " + getMarca() + "\n"
				+ "Modelo: " + getModelo() + "\n"
				+ "Preço: " + getPreco() + "\n"
				+ "Ano: " + getAno() + "\n"
				+ "Novo/Usado: " + getNovoUsado() + "\n"
				+ "Cor: " + getCor() + "\n"
				+ "Quantidade de Rodas: " + getQuantRodas() + "\n"
				+ "Peso: " + getPeso() + "\n"
				+ "Chassi: " + getNumeroChassi() + "\n";
				
				
	}
	

	public Integer getQuantRodas() {
		return quantRodas;
	}


	public void setQuantRodas(Integer quantRodas) {
		this.quantRodas = quantRodas;
	}


	public Double getPeso() {
		return peso;
	}


	public void setPeso(Double peso) {
		this.peso = peso;
	}


	public String getCor() {
		return cor;
	}


	public void setCor(String cor) {
		this.cor = cor;
	}


	public Integer getAno() {
		return ano;
	}


	public void setAno(Integer ano) {
		this.ano = ano;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public String getModelo() {
		return modelo;
	}


	public void setModelo(String modelo) {
		this.modelo = modelo;
	}


	public NovoUsado getNovoUsado() {
		return novoUsado;
	}


	public void setNovoUsado(NovoUsado novoUsado) {
		this.novoUsado = novoUsado;
	}


	public String getNumeroChassi() {
		return numeroChassi;
	}


	public void setNumeroChassi(String numeroChassi) {
		this.numeroChassi = numeroChassi;
	}


	public Double getPreco() {
		return preco;
	}


	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
	
	
	

}
