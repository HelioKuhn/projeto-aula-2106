package br.com.syonet.concessionaria;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		/**
		 * public Carro(Integer quantRodasInteger, Double peso, String cor, Integer ano, String marca, String modelo, Double preco,
			NovoUsado novoUsado, String numeroChassi, TipoCombustivel tipoCombustivel, Boolean direcaoHidraulica, Integer quantidadePassageiros, TipoCambio tipoCambio) {
		super(quantRodasInteger, peso, cor, ano, marca, modelo, preco, novoUsado, numeroChassi);
		 */
		
		Concessionaria concessionaria = new ConcessionariaImpli();
		
		Carro carro = new Carro(4, 7500.50, "Branco", 2022, "HONDA", "CIVIC", 78000.00,
				NovoUsado.NOVO, "1234", TipoCombustivel.GALOSINA, true, 5, TipoCambio.AUTOMATICO);
		
		concessionaria.adicionarVeiculoEstoque(carro);
		
		carro = new Carro(4, 7500.50, "Preto", 2018, "VW", "GOL", 45700.00,
				NovoUsado.SEMINOVOS, "43GH", TipoCombustivel.GALOSINA, true, 5, TipoCambio.MANUAL);
		
		Moto moto = new Moto(2, 2000.50, "AZUL", 2021, "YAMAHA", "FASER", 9800.50,
				NovoUsado.SEMINOVOS, "18HJ", true, TipoMoto.CUSTON, 250);
		
		concessionaria.adicionarVeiculoEstoque(moto);
		
		moto = new Moto(2, 2000.50, "ROSA", 2021, "HONDA", "FAN", 7000.50,
				NovoUsado.NOVO, "235H", true, TipoMoto.STREET, 125);
		
		concessionaria.adicionarVeiculoEstoque(moto);
		
		moto = new Moto(2, 2000.50, "AZUL", 2021, "HONDA", "FASER", 12000.50,
				NovoUsado.SEMINOVOS, "FHOS", true, TipoMoto.ESPORTIVA, 250);
		
		concessionaria.adicionarVeiculoEstoque(moto);
		
		concessionaria.adicionarVeiculoEstoque(carro);
		
		List<Moto> listaMotosCilindradas = concessionaria.listarMotosCilindrada(250);
		
		System.out.println("Motos por Cilindrada");
		for(Moto m: listaMotosCilindradas) {
			System.out.println(m);
		}
		
		List<Carro> carrosNovos = concessionaria.listarCarros(NovoUsado.AMBOS);
		
		concessionaria.atualizarPrecoVenda(carro, 12000.00);
		
		concessionaria.venderVeiculo(carro, TipoCliente.COMUM);
		
		
		List<Moto> listaMotosMarca = concessionaria.listarMotosMarca("HONDA");
		
		System.out.println("Motos por marca");
		for(Moto m: listaMotosMarca) {
			System.out.println(m);
		}
		
		System.out.println("Listar Carros novos");
		for(Carro c: carrosNovos) {
			System.out.println(c);
		}
		
		concessionaria.listarVeiculosVendidos();
		
		
		Caminhao caminhao = new Caminhao(18, 150000.00, "ROXO", 2021, "SCANNIA", "XXX", 450000.00,
				NovoUsado.SEMINOVOS, "XJJ88D", TipoCarroceriaCaminhao.BAU,12);
		
		concessionaria.adicionarVeiculoEstoque(caminhao);
		
		List<Caminhao> listarCaminhaoCarroceria = concessionaria.listarCaminhoes(TipoCarroceriaCaminhao.BAU);
		
		System.out.println("Listar caminhões por tipo da carroceria");
		for(Caminhao ca: listarCaminhaoCarroceria) {
			System.out.println(ca);
		}

	}

}
