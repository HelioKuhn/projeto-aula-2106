package br.com.syonet.concessionaria;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ConcessionariaImpli implements Concessionaria{
	
	private static List<Veiculo> listaVeiculosEstoque = new ArrayList<Veiculo>();
	
	private static List<Veiculo> listaVeiculosVendidos = new ArrayList<Veiculo>();

	@Override
	public void adicionarVeiculoEstoque(Veiculo veiculo) {
		listaVeiculosEstoque.add(veiculo);
		
	}

	@Override
	public void removerVeiculoEstoque(Veiculo veiculo) {
		Iterator<Veiculo> it = listaVeiculosEstoque.iterator();
		while(it.hasNext()) {
			Veiculo v = it.next();
			if(v.getNumeroChassi() == veiculo.getNumeroChassi()) {
				it.remove();
			}
		}
		
	}

	@Override
	public void atualizarPrecoVenda(Veiculo veiculo, Double preco) {
		for(Veiculo v: listaVeiculosEstoque) {
			if(v.getNumeroChassi() == veiculo.getNumeroChassi()) {
				v.setPreco(preco);
			}
		}
		
	}

	@Override
	public void venderVeiculo(Veiculo veiculo, TipoCliente tipoCliente) {
		Iterator<Veiculo> it = listaVeiculosEstoque.iterator();
		while(it.hasNext()) {
			Veiculo v = it.next();
			if(v.getNumeroChassi()== veiculo.getNumeroChassi() && tipoCliente == TipoCliente.PCD) {
				v.setPreco((v.getPreco()*0.5));
				listaVeiculosVendidos.add(v);
				it.remove();
				return;
			}
			
			if(v.getNumeroChassi()== veiculo.getNumeroChassi() && tipoCliente == TipoCliente.AGRICULTOR) {
				v.setPreco((v.getPreco()*0.6));
				listaVeiculosVendidos.add(v);
				it.remove();
				return;
			}
			
			if(v.getNumeroChassi()== veiculo.getNumeroChassi() && tipoCliente == TipoCliente.COMUM) {
				listaVeiculosVendidos.add(v);
				it.remove();
				return;
			}
			
		}
		
		
	}

	@Override
	public List<Carro> listarCarros(NovoUsado novoUsado) {
		
		
		if(novoUsado == NovoUsado.NOVO) {
			List<Carro> veliculosNovos = new ArrayList<Carro>();
			for(Veiculo v: listaVeiculosEstoque) {
				if(v.getNovoUsado() == novoUsado && v instanceof Carro) {
					Carro carro = (Carro) v;
					veliculosNovos.add(carro);
				}
			}
			return veliculosNovos;
		}
		else if(novoUsado == NovoUsado.SEMINOVOS) {
			List<Carro> veliculosSeminovos = new ArrayList<Carro>();
			for(Veiculo v: listaVeiculosEstoque) {
				if(v.getNovoUsado() == novoUsado && v instanceof Carro) {
					Carro carro = (Carro) v;
					veliculosSeminovos.add(carro);
				}
			}
			return veliculosSeminovos;
		}
		
		else{
			List<Carro> veliculosAmbos = new ArrayList<Carro>();
			for(Veiculo v: listaVeiculosEstoque) {
				if(v instanceof Carro) {
					Carro carro = (Carro) v;
					veliculosAmbos.add(carro);
				}
			}
			return veliculosAmbos;
		}
		
	}

	@Override
	public List<Moto> listarMotosMarca(String marca) {
		List<Moto> listaMotoMarca = new ArrayList<Moto>();
		for(Veiculo v: listaVeiculosEstoque) {
			if(marca.equals(v.getMarca()) && v instanceof Moto) {
				Moto moto= (Moto)v;
				listaMotoMarca.add(moto);
			}
		}
		return listaMotoMarca;
	}

	@Override
	public List<Moto> listarMotosCilindrada(Integer cilindrada) {
		List<Moto>listaMotoCilindradas = new ArrayList<Moto>();
		for(Veiculo v: listaVeiculosEstoque) {
			if(v instanceof Moto) {
				Moto moto = (Moto)v;
				if(moto.getTipoCilindrada().equals(cilindrada)) {
					listaMotoCilindradas.add(moto);
				}
				
			}
		}
		return listaMotoCilindradas;
	}

	@Override
	public List<Caminhao> listarCaminhoes(TipoCarroceriaCaminhao tipoCarroceriaCaminhao) {
		List<Caminhao>listaCaminhaoTipoCarroceria = new ArrayList<Caminhao>();
		for(Veiculo v: listaVeiculosEstoque) {
			if(v instanceof Caminhao) {
				Caminhao caminhao = (Caminhao)v;
				if(caminhao.getTipoCarroceriaCaminhao() == tipoCarroceriaCaminhao) {
					listaCaminhaoTipoCarroceria.add(caminhao);
				}
				
			}
		}
		return listaCaminhaoTipoCarroceria;
	}
	
	
	
	public void listarVeiculosVendidos() {
		System.out.println("Lista de veículos Vendidos");
		for(Veiculo v: listaVeiculosVendidos) {
			System.out.println(v);
		}
	}

}
