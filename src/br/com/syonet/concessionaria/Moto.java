package br.com.syonet.concessionaria;

public class Moto extends Veiculo{
	
	
	private Boolean freioDisco;
	private TipoMoto tipoMoto;
	private Integer tipoCilindrada;
	
	public Moto(Integer quantRodasInteger, Double peso, String cor, Integer ano, String marca, String modelo,
			Double preco, NovoUsado novoUsado, String numeroChassi, Boolean freioDisco, TipoMoto tipoMoto, Integer tipoCilindrada) {
		super(quantRodasInteger, peso, cor, ano, marca, modelo, preco, novoUsado, numeroChassi);
		this.freioDisco = freioDisco;
		this.tipoMoto = tipoMoto;
		this.tipoCilindrada = tipoCilindrada;
		
	}
		
		public String toString() {
			return "Moto\n"
			+ super.toString()
			+"Freio a disco: " + getFreioDisco() + "\n"
			+ "Tipo: " + getTipoMoto() + "\n"
			+ "Tipo Cilindradas: " + getTipoCilindrada()
			+ "\n\n";
		}
	

	public Integer getTipoCilindrada() {
		return tipoCilindrada;
	}

	public void setTipoCilindrada(Integer tipoCilindrada) {
		this.tipoCilindrada = tipoCilindrada;
	}

	public String getFreioDisco() {
		return freioDisco == true ? "SIM" : "NÃO";
	}

	public void setFreioDisco(Boolean freioDisco) {
		this.freioDisco = freioDisco;
	}

	public TipoMoto getTipoMoto() {
		return tipoMoto;
	}

	public void setTipoMoto(TipoMoto tipoMoto) {
		this.tipoMoto = tipoMoto;
	}

	
	

}
