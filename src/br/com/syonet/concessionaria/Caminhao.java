package br.com.syonet.concessionaria;

public class Caminhao extends Veiculo{
	
	private TipoCarroceriaCaminhao tipoCarroceriaCaminhao;
	private Integer quantidadeEixo;

	public Caminhao(Integer quantRodasInteger, Double peso, String cor, Integer ano, String marca, String modelo,
			Double preco, NovoUsado novoUsado, String numeroChassi,TipoCarroceriaCaminhao tipoCarroceriaCaminhao, Integer quantidadeEixo) {
		super(quantRodasInteger, peso, cor, ano, marca, modelo, preco, novoUsado, numeroChassi);
		this.tipoCarroceriaCaminhao = tipoCarroceriaCaminhao;
		this.quantidadeEixo = quantidadeEixo;
	}
	
	public String toString() {
		return "Caminhão\n"
				+super.toString() 
				+"Tipo de Carroceria: "+ getTipoCarroceriaCaminhao() +"\n"
				+"Quantidade de Eixos: " + getQuantidadeEixo()
				+"\n\n";
	}

	public TipoCarroceriaCaminhao getTipoCarroceriaCaminhao() {
		return tipoCarroceriaCaminhao;
	}

	public void setTipoCarroceriaCaminhao(TipoCarroceriaCaminhao tipoCarroceriaCaminhao) {
		this.tipoCarroceriaCaminhao = tipoCarroceriaCaminhao;
	}

	public Integer getQuantidadeEixo() {
		return quantidadeEixo;
	}

	public void setQuantidadeEixo(Integer quantidadeEixo) {
		this.quantidadeEixo = quantidadeEixo;
	}
	
	
	
}
