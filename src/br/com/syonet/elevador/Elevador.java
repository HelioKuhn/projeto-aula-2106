package br.com.syonet.elevador;

public interface Elevador {
	
	void inicializa(Integer capacidade, Integer totalAndaresPredio);
	
	void entra();
	
	void sai();
	
	void sobe();
	
	void desce();

}
