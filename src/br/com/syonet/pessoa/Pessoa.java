package br.com.syonet.pessoa;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Pessoa {

	private String nome;
	private Integer idade;
	private Double altura;
	private LocalDate dataNascimento;
	private DateTimeFormatter sdf = DateTimeFormatter.ofPattern("dd/MM/yyyy"); 
	

	public Pessoa(String nome, String dataNascimento, Double altura) throws ParseException {
		super();
		this.nome = nome;
		this.dataNascimento =  LocalDate.parse(dataNascimento, sdf);
		calculaIdade(LocalDate.parse(dataNascimento, sdf));
		this.altura = altura;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getAutura() {
		return altura;
	}

	public void setAutura(Double altura) {
		this.altura = altura;
	}

	public Integer getIdade() {
		return idade;
	}
	
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento =  LocalDate.parse(dataNascimento, sdf);
		calculaIdade(getDataNascimento());
	}
	
	
	public String toString() {
		return	"\n" +
				"----Dados do(a)" + getNome() + "----\n"
				+ "Nome: " + getNome()+ "\n"
				+ "Data Nascimento: " + getDataNascimento().format(sdf) + "\n"
				+ "Idade: " + getIdade()+ " anos\n"
				+ "Altura: " + getAutura();
	}
	
	public void calculaIdade(LocalDate dataNascimento) {
		LocalDate dataAtual = LocalDate.now();
		
		if(dataAtual.getMonth().getValue() > dataNascimento.getMonthValue()) {
			this.idade = dataAtual.getYear() - dataNascimento.getYear();
			return;
		}
		if(dataAtual.getMonth().getValue() == dataNascimento.getMonthValue() && dataAtual.getDayOfMonth() >= dataNascimento.getDayOfMonth() ) {
			this.idade = dataAtual.getYear() - dataNascimento.getYear();
		}else {
			this.idade = dataAtual.getYear() - dataNascimento.getYear() -1;
		}
	}
	
	
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	
	

}
