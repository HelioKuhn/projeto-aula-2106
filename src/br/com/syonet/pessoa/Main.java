package br.com.syonet.pessoa;

import java.text.ParseException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		
		System.out.println("----Dados da Pessoa----");
		
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Nome: ");
		String nome = sc.nextLine();
		
		System.out.print("DataNascimento(dd/mm/yyyy): ");
		String dataNascimento = sc.nextLine();
		
		System.out.print("Altura: ");
		double altura = sc.nextDouble();
		
		Pessoa p = new Pessoa(nome, dataNascimento, altura);
		
		System.out.println(p);
		
		sc.close();
	}

}
